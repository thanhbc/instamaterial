# This project will demo about Android architecture at PrimeLabo VN

Follow [Android-CleanArchitecture](http://fernandocejas.com/2014/09/03/architecting-android-the-clean-way/)  article

This is Instagram concept with material design for demo MVP UI pattern approach in Android development.
  - [x] MVP UI pattern.
  - [ ] Clean architecture.
  - [ ] Dagger 2 for Dependency Injection

How to use
-----------------

Checkout `master` branch for boilerplate code and improve it with MVP approach.

Checkout `mvp` branch for MVP implementation
> checkout tag `homework` and `DIY`

If you find difficult please checkout `finish_mvp` for full MVP implementation

*[WIP]* : Clean architecture

  ![Video Walkthrough](instagram_concept.gif)
