package com.thanhbc.instamaterial.views;

import com.thanhbc.instamaterial.views.adapters.FeedAdapter;

import java.util.List;

/**
 * Created by thanhbc on 2/1/16.
 */
public interface MainView extends CommonView {
    void displayFeed(List<FeedAdapter.FeedItem> items);
}
