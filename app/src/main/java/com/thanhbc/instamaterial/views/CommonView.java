package com.thanhbc.instamaterial.views;

import android.content.Context;

/**
 * Created by thanhbc on 2/1/16.
 */
public interface CommonView {
    Context getContext();
}
