package com.thanhbc.instamaterial.presenters;

/**
 * Created by thanhbc on 2/1/16.
 */
public interface Presenter<V> {
    void attachView(V view);
    void dettachView();
}
