package com.thanhbc.instamaterial.presenters.abstracts;

import com.thanhbc.instamaterial.presenters.Presenter;
import com.thanhbc.instamaterial.views.MainView;

/**
 * Created by thanhbc on 2/1/16.
 */
public interface MainPresenter extends Presenter<MainView> {
    void loadData();
}
