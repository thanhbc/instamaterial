package com.thanhbc.instamaterial.presenters.iteractors;

import android.view.View;

/**
 * Created by thanhbc on 2/1/16.
 */
public interface OnFeedProfileClicked extends FeedIteractor {
    void onProfileClicked(View v);
}
