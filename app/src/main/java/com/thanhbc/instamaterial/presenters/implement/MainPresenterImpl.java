package com.thanhbc.instamaterial.presenters.implement;

import android.view.View;

import com.thanhbc.instamaterial.presenters.abstracts.MainPresenter;
import com.thanhbc.instamaterial.presenters.iteractors.OnFeedCommentClicked;
import com.thanhbc.instamaterial.presenters.iteractors.OnFeedLikeClicked;
import com.thanhbc.instamaterial.presenters.iteractors.OnFeedProfileClicked;
import com.thanhbc.instamaterial.presenters.iteractors.OnFeedShowMoreClicked;
import com.thanhbc.instamaterial.views.MainView;

/**
 * Created by thanhbc on 2/1/16.
 */
public class MainPresenterImpl implements MainPresenter,
        OnFeedCommentClicked,
        OnFeedLikeClicked,
        OnFeedProfileClicked,
        OnFeedShowMoreClicked{


    @Override
    public void loadData() {

    }

    @Override
    public void onCommentClicked(View v, int position) {

    }

    @Override
    public void onLikeClick(View v, int position) {

    }

    @Override
    public void onProfileClicked(View v) {

    }

    @Override
    public void onMoreClicked(View v, int position) {

    }

    @Override
    public void attachView(MainView view) {

    }

    @Override
    public void dettachView() {

    }
}
