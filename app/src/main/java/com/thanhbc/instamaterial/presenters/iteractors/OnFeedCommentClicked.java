package com.thanhbc.instamaterial.presenters.iteractors;

import android.view.View;

/**
 * Created by thanhbc on 2/1/16.
 */
public interface OnFeedCommentClicked extends FeedIteractor {
    void onCommentClicked(View v, int position);
}
