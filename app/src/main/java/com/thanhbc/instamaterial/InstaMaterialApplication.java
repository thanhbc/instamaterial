package com.thanhbc.instamaterial;

import android.app.Application;

import timber.log.Timber;

/**
 * Created by thanhbc on 1/28/16.
 */
public class InstaMaterialApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
    }
}